package com.workday.tests;

import static com.workday.testengineering.html.core.HtmlPageExpressions.loaded;

import javax.inject.Inject;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.workday.html.tasks.ApprovePositionTask;
import com.workday.html.tasks.CreatePositionTask;
import com.workday.testbase.LotusTest;
import com.workday.testengineering.html.pages.LotusLandingPage;

public class PositionTests extends LotusTest {
	private static final String CREATING_A_NEW_POSITION = "Creating a new position";
	private static final String REGULAR = "Regular";
	private static final String EMPLOYEE = "Employee";
	private static final String FULL_TIME = "Full time";
	private static final String SAN_FRANCISCO = "San Francisco";
	private static final String DIRECTOR_FACILITIES_OPERATIONS = "Director, Facilities Operations";
	private static final String DATE = "06/21/2011";
	private static final String ONE = "1";
	private static final String JOB_DESCRIPTION = "This is a job description";
	private static final String JOB_TITLE = "Test Job Title";
	private static final String A_MANAGER = "jtaylor";
	private static final String AN_HR_PARTNER = "lmcneil";
	private static final String IT_HELPDESK_DEPARTMENT = "IT HelpDesk Department";
	private static final String ADMINISTRATION = "Job Families";
	private static final String FA_ADMINISTRATION = "FA-Administration";
	private static final int FIRST_OCCURANCE = 1;


	@Inject private CreatePositionTask createPositionTask;
	@Inject private ApprovePositionTask approvePositionTask;
	@Inject private LotusLandingPage landingPage;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// The After Method tears down the test, logging out the user. 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@AfterMethod(alwaysRun=true)
	public void signOut(){
		safelySignOut();
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// The test creates a position within the workday application.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test (groups = { "exampleTest" })
	public void createPosition() {
		logInAs(A_MANAGER);
		openCreatePositionPageFor(IT_HELPDESK_DEPARTMENT);
		submitCreatePosition();
		verifyPositionIsCreated();
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// The test approves the new position that was just created.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Test (groups = { "exampleTest" }, dependsOnMethods={"createPosition"})
	public void approvePosition() {
		logInAs(AN_HR_PARTNER);
		approvePositionCreation();
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Test helper methods.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	private void logInAs(String username) {
		loginAs(username);
		waitUntil(landingPage, is(loaded()));
	}	
	
	private void openCreatePositionPageFor(String organization) {
		createPositionTask
			.initiateFromSearch()
			.onPositionCreateStartPage()
			.verifySelectedSupervisoryOrganization(organization)
			.ok();
	}
	
	private void submitCreatePosition() {
		createPositionTask
			.onPositionCreatePage()
				.enterJobPostingTitle(JOB_TITLE)
				.enterNumberOfPositions(ONE)
				.enterJobDescription(JOB_DESCRIPTION)
				.enterAvailabilityDate(DATE)
				.enterEarliestHireDate(DATE)
				.uncheckNoJobRestrictions()
				.selectJobFamily(ADMINISTRATION, FA_ADMINISTRATION)
				.selectJobProfile(DIRECTOR_FACILITIES_OPERATIONS)
				.selectLocation(SAN_FRANCISCO)
				.selectTimeType(FULL_TIME)
				.selectWorkerType(EMPLOYEE)
				.selectWorkerSubType(REGULAR).enterComment(CREATING_A_NEW_POSITION)
				.submit();
	}
	
	private void approvePositionCreation() {
		approvePositionTask
			.initiateFromInbox("Create Position: Test Job Title", FIRST_OCCURANCE)
				.onApproveCreatePositionEditPage()
					.approve("I approve of this position");
		approvePositionTask
			.onPositionCreateConfirmationPage();
	}
	
	private void verifyPositionIsCreated() {
		String availabilityDate = createPositionTask
			.onPositionCreateConfirmationPage().openDetails().availabilityDate();
		assertThat(availabilityDate, is(DATE));
	}
}

	