package com.workday.tests;

import static com.workday.testengineering.html.core.HtmlPageExpressions.loaded;

import javax.inject.Inject;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.workday.html.tasks.RequestCompensationChangeTask;
import com.workday.testbase.LotusTest;
import com.workday.testengineering.html.pages.LotusLandingPage;

public class ChangeCompensationTests extends LotusTest{

	private static final String NEW_SALARY = "142000";
	private static final String EFFECTIVE_DATE = "11/08/2014";
	private static final String BETTY_LIU = "Betty Liu";
	private static final String BETTY_LIU_ID = "247$8";
	private static final By ROOT = By.xpath("//*[not(./ancestor::*[@id='wd-PageHeader-null'])]");
	private static final String REQUEST_COMPENSATION_CHANGE_ADJUSTMENT = "Request Compensation Change > Adjustment";
	private static final String ADJUSTMENT_JOB_CHANGE = "Adjustment > Job Change";
	private static final String A_MANAGER = "lmcneil";
	
	@Inject private RequestCompensationChangeTask requestCompensationChangeTask;
	@Inject private LotusLandingPage landingPage;
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//The After Method tears down the test, logging out the user. 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@AfterMethod(alwaysRun=true)
	public void signOut() {
		safelySignOut();
	}	
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//The test changes the salary, demonstrating interacting with an orchestration row with multiple columns. (An orchestration page)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test (groups = { "exampleTest" })
	public void changeCompensation() {
		logInAs(A_MANAGER);
		navigateToRequestCompensationChangeForEmployee();
		changeSalary();
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Test helper methods.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private void logInAs(String username) {
		loginAs(username);
		waitUntil(landingPage, is(loaded()));
	}
	
	private void navigateToRequestCompensationChangeForEmployee() {
		navigateToEmployeeRequestCompensationChangePage();
		setCompensationChangeDateAndReason();
	}

	private void setCompensationChangeDateAndReason() {
		requestCompensationChangeTask
			.onEmployeeRequestAdhocCompensationChangeStartPage()
				.enterEffectiveDate(EFFECTIVE_DATE)
				.selectReason(REQUEST_COMPENSATION_CHANGE_ADJUSTMENT, ADJUSTMENT_JOB_CHANGE)
				.ok();
	}

	private void navigateToEmployeeRequestCompensationChangePage() {
		requestCompensationChangeTask
			.initiateFromProfileRelatedAction(ROOT, BETTY_LIU_ID, BETTY_LIU)
				.onEmployeeRequestAdhocCompensationChangeStartPage();
	}

	private void changeSalary() {
		requestCompensationChangeTask
			.onRequestCompensationChangeForOrchestrationViewPage()
				.enterSalary(NEW_SALARY)
				.done();
		requestCompensationChangeTask
			.onRequestCompensationChangeForOrchestrationViewPage()
				.submit();
		requestCompensationChangeTask
			.onRequestCompensationChangeConfirmationPage();
	}
}