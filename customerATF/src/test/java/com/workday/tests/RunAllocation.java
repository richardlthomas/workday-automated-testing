package com.workday.tests;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RunAllocation {

//	public static WebDriver chromeDriver;
	public static WebDriver firefoxDriver;
//	public static WebDriver iexploreDriver;
//	@Inject private WorkdayWebDriver workdayDriver;
	public final static String URL = "https://wd5-impl.workday.com/Rochester2/login.flex?redirect=n";
	public static WebDriverWait wait;
	
	private static final String ACCOUNTANT = "wd_ldap_test01";
	private static final String ACCT_PASS = "Wdtest0101";
	private static final String TRIAL_BAL = "trial bal";
	private static final String SCHED_ALLOC = "sched alloc";
	private static final String COMPANY_ALLOC = "010";
	private static final String FISC_YEAR = "FY2014";
	
	private static WebElement usernameField;
	private static WebElement passwordField;
//	private static WebElement loginButton;
	private static WebElement searchBox;
	private static WebElement trialBalLink;
	private static WebElement ledgerBox;
	private static WebElement yearBox;
	private static WebElement wdAppFrame;
	private static WebElement searchResults;
	private static WebElement continueButton;
	private static List<WebElement> promptInputs;
	private static List<WebElement> promptOptions;
	
	@BeforeMethod(alwaysRun = true)
	public void initialize() throws InterruptedException{
//		System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
//		chromeDriver = new ChromeDriver();
//		chromeDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		wait = new WebDriverWait(chromeDriver, 10);
//		TestNGThread.sleep(5000);
		firefoxDriver = new FirefoxDriver();
		firefoxDriver.manage().window().maximize();
		firefoxDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		wait = new WebDriverWait(firefoxDriver, 10);
//		System.setProperty("webdriver.ie.driver", "C:\\Program Files (x86)\\Internet Explorer\\iexplore.exe");
//		iexploreDriver = new InternetExplorerDriver();
//		iexploreDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		wait = new WebDriverWait(iexploreDriver, 10);
////		workdayDriver = new WorkdayWebDriver();
//		iexploreDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		wait = new WebDriverWait(iexploreDriver, 10);
		System.out.println("URL loaded");
	}
	
	@Test (groups = { "exampleTest" })
	public void runAllocation() throws InterruptedException {
		logInAs(ACCOUNTANT, ACCT_PASS);
		searchFor(TRIAL_BAL);
		selectTrialBal();
		selectAlloc(COMPANY_ALLOC, FISC_YEAR);
		searchFor(SCHED_ALLOC);
	}
	
	private void logInAs(String user, String pass) {
//		chromeDriver.get(URL);
		firefoxDriver.get(URL);
//		iexploreDriver.get(URL);
		usernameField = findElement("cssSelector", "input[automationid=userName]");
		System.out.println("Found username field: " + usernameField.toString());
		passwordField = findElement("cssSelector", "input[automationid=password]");
		System.out.println("Found password field: " + passwordField.toString());
//		loginButton = findElement("cssSelector", "input[automationid=goButton]");
//		System.out.println("Found login button: " + loginButton.toString());
		usernameField.sendKeys(user);
		passwordField.sendKeys(pass);
		passwordField.sendKeys(Keys.ENTER);
//		loginButton.click();
		//waitUntil(landingPage, is(loaded()));
	}
	
	private void searchFor(String searchTerms) {
		searchBox = findElement("cssSelector", "input[automationid=globalSearchInput]");
		searchBox.clear();
		searchBox.sendKeys(searchTerms);
		searchBox.sendKeys(Keys.RETURN);
	}
	
	private void selectTrialBal() {
		searchResults = findElement("id", "wd-SearchResult");
		System.out.println("Found searchResults: " + searchResults.toString());
		promptOptions = searchResults.findElements(By.cssSelector("div[automationid=promptOption]"));
		System.out.println("search results: " + promptOptions.toString());
		trialBalLink = promptOptions.get(0);
		System.out.println("Found trial balance link: " + trialBalLink.toString());
		trialBalLink.click();
	}
	
	private void selectAlloc(String company, String year) throws InterruptedException {
		wdAppFrame = findElement("id", "workdayApplicationFrame");
		promptInputs = wdAppFrame.findElements(By.cssSelector("input[automationid=promptInput]"));
		System.out.println("journal inputs: " + promptInputs.toString());
		ledgerBox = promptInputs.get(0);
		System.out.println("Found ledger box: " + ledgerBox.toString());
		ledgerBox.sendKeys(company);
		ledgerBox.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		yearBox = promptInputs.get(3);
		yearBox.sendKeys(year);
		yearBox.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		continueButton = findElement("cssSelector", "a[automationid=wd-CommandButton_SECONDARY__SUBMIT_TaskId_Edit_Page]");
		continueButton.click();
		Thread.sleep(3000);
	}
	
	public static WebElement findElement(String byType, String selector){
		switch (byType){
		case "className":
			return wait.until(ExpectedConditions.elementToBeClickable(By.className(selector)));
		case "cssSelector":
			return wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(selector)));
		case "id":
			return wait.until(ExpectedConditions.elementToBeClickable(By.id(selector)));
		case "linkText":
			return wait.until(ExpectedConditions.elementToBeClickable(By.linkText(selector)));
		case "name":
			return wait.until(ExpectedConditions.elementToBeClickable(By.name(selector)));
		case "partialLinkText":
			return wait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText(selector)));
		case "tagName":
			return wait.until(ExpectedConditions.elementToBeClickable(By.tagName(selector)));
		case "xpath":
			return wait.until(ExpectedConditions.elementToBeClickable(By.xpath(selector)));
		}
		return wait.until(ExpectedConditions.elementToBeClickable(By.className("headerImgBOT")));
	}
	
	@AfterMethod(alwaysRun=true)
	public void signOut(){
//		chromeDriver.quit();
		firefoxDriver.quit();
//		iexploreDriver.quit();
	}
	
}
