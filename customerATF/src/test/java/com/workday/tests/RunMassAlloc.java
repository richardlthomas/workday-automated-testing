package com.workday.tests;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class RunMassAlloc {

//	public static WebDriver chromeDriver;
	public static WebDriver firefoxDriver;
//	public static WebDriver iexploreDriver;
//	@Inject private WorkdayWebDriver workdayDriver;
	public final static String URL = "https://wd5-impl.workday.com/Rochester2/login.flex?redirect=n";
	public static WebDriverWait wait;
	
	private static final String DATA_ENTRY_SPEC = "wd_ldap_test01";
	private static final String DES_PASS = "Wdtest0101";
	private static final String CRE_SUP_INV = "cre sup inv";
	private static final String COMPANY = "010";
	private static final String SUPPLIER = "Bagel Land";
	private static final String AMOUNT = "1000.00";
	private static final String INVOICE_WORKTAG = "SC49650";
//	private static final String[] INVOICE_WORKTAGS = {"SC49650"};
	
	private static WebElement usernameField;
	private static WebElement passwordField;
//	private static WebElement loginButton;
	private static WebElement searchBox;
	private static WebElement createInvoiceLink;
	private static WebElement companyBox;
	private static WebElement supplierBox;
	private static WebElement tableRow;
	private static WebElement amountCell;
	private static WebElement amountBox;
	private static WebElement worktagCell;
	private static WebElement worktagBox;
	private static WebElement wdAppFrame;
	private static WebElement searchResults;
	private static WebElement continueButton;
	private static List<WebElement> promptInputs;
	private static List<WebElement> promptOptions;
	private static List<WebElement> tableCells;
	
	@BeforeMethod(alwaysRun = true)
	public void initialize() throws InterruptedException{
//		System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
//		chromeDriver = new ChromeDriver();
//		chromeDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		wait = new WebDriverWait(chromeDriver, 10);
//		TestNGThread.sleep(5000);
		firefoxDriver = new FirefoxDriver();
//		firefoxDriver.manage().window().maximize();
		firefoxDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		wait = new WebDriverWait(firefoxDriver, 15);
//		System.setProperty("webdriver.ie.driver", "C:\\Program Files (x86)\\Internet Explorer\\iexplore.exe");
//		iexploreDriver = new InternetExplorerDriver();
//		iexploreDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		wait = new WebDriverWait(iexploreDriver, 10);
//		workdayDriver = new WorkdayWebDriver();
//		iexploreDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		wait = new WebDriverWait(iexploreDriver, 10);
		System.out.println("URL loaded");
	}
	
	private void logInAs(String user, String pass) {
//		chromeDriver.get(URL);
		firefoxDriver.get(URL);
//		iexploreDriver.get(URL);
		usernameField = findElement("cssSelector", "input[automationid=userName]");
		System.out.println("Found username field: " + usernameField.toString());
		passwordField = findElement("cssSelector", "input[automationid=password]");
		System.out.println("Found password field: " + passwordField.toString());
//		loginButton = findElement("cssSelector", "input[automationid=goButton]");
//		System.out.println("Found login button: " + loginButton.toString());
		usernameField.sendKeys(user);
		passwordField.sendKeys(pass);
		passwordField.sendKeys(Keys.ENTER);
//		loginButton.click();
		//waitUntil(landingPage, is(loaded()));
	}
	
	private void searchFor(String searchTerms) {
		searchBox = findElement("cssSelector", "input[automationid=globalSearchInput]");
		searchBox.clear();
		searchBox.sendKeys(searchTerms);
		searchBox.sendKeys(Keys.RETURN);
	}
	
	public static WebElement findElement(String byType, String selector){
		switch (byType){
		case "className":
			return wait.until(ExpectedConditions.elementToBeClickable(By.className(selector)));
		case "cssSelector":
			return wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(selector)));
		case "id":
			return wait.until(ExpectedConditions.elementToBeClickable(By.id(selector)));
		case "linkText":
			return wait.until(ExpectedConditions.elementToBeClickable(By.linkText(selector)));
		case "name":
			return wait.until(ExpectedConditions.elementToBeClickable(By.name(selector)));
		case "partialLinkText":
			return wait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText(selector)));
		case "tagName":
			return wait.until(ExpectedConditions.elementToBeClickable(By.tagName(selector)));
		case "xpath":
			return wait.until(ExpectedConditions.elementToBeClickable(By.xpath(selector)));
		}
		return wait.until(ExpectedConditions.elementToBeClickable(By.className("headerImgBOT")));
	}
	
	@AfterMethod(alwaysRun=true)
	public void signOut(){
//		chromeDriver.quit();
		firefoxDriver.quit();
//		iexploreDriver.quit();
	}
	
}
