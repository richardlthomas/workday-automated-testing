package com.workday.tests;

import static com.dhemery.slicer.Slicer.slice;
import static com.workday.testengineering.html.core.HtmlPageExpressions.loaded;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Iterator;

import javax.inject.Inject;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.workday.html.tasks.RequestTimeOffTask;
import com.workday.testbase.LotusTest;
import com.workday.testengineering.html.pages.LotusLandingPage;

public class DataDrivenTimeOffTests extends LotusTest {
	
	private static final int EIGHT_HOURS = 8;
	private static final String PTO_SICK_PROMPT_SEARCH = "Sick";
	private static final String PTO_SICK_PROMPT_OPTION = "Sick (Hours)";
	private static final String PTO_VACATION_PROMPT_SEARCH = "Vacation";
	private static final String PTO_VACATION_PROMPT_OPTION = "Vacation (Hours)";

	@Inject private RequestTimeOffTask requestTimeOffTask;
	@Inject private LotusLandingPage landingPage;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This is how to use a DataProvider within the test itself.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	String[][] timeOffCells = new String[][]{
			{"lmcneil",	"17", PTO_VACATION_PROMPT_SEARCH, PTO_VACATION_PROMPT_OPTION},
			{"lmcneil",	"16", PTO_SICK_PROMPT_SEARCH, PTO_SICK_PROMPT_OPTION}
	};
	
	@DataProvider
	public Iterator<Object[]> cellsTest(Method method){
		return slice(timeOffCells).asParametersFor(method);
	}	
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This is how to use a DataProvider with a csv file. The csv file needs to be located within the same package as the test.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	@DataProvider
	public Iterator<Object[]> csvTest(Method method) throws IOException {
		String filename = getClass().getResource("timeOffType.csv").getPath();
		return slice(filename).asParametersFor(method);
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// The After Method tears down the test, logging out the user. 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@AfterMethod(alwaysRun=true)
	public void signOut(){
		safelySignOut();
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// The test then sets the specified DataProvider and sets the values used in the test. In this example it is set up 
// to use the csv file for the data provider. The csv file can be found in src/test/resources > com.workday.tests
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
	@Test (groups = { "exampleTest" }, dataProvider="cellsTest")
	public void requestTimeOff(String user, int date, String timeOffType, String timeOffTypeResult){
		logInAs(user);
		openRequestTimeOffPage();
		requestOneDayOff(date, timeOffType, timeOffTypeResult);
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Test helper methods.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Login is now handled with html, changes have been made in the framework.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	private void logInAs(String username) {
		loginAs(username);
		waitUntil(landingPage, is(loaded()));
	}	

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Search is still in flex, these commands will stay the same, however if an edit page is opened from search
// the commands will need to switch to html to wait for the correct object 	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	private void openRequestTimeOffPage() {
		requestTimeOffTask
			.initiateFromSearch();
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// html method declarations for specified pages can be found in com.workday.html.pages
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	private void requestOneDayOff(int date, String promptSearch, String promptResult) {
		requestTimeOffTask
			.onRequestTimeOffCalendarPage()
				.clickSelectDayOfMonth(date)
				.requestSelectedTimeOff();
		requestTimeOffTask
			.onRequestTimeOffMicroEditPage()
				.selectRequestType(promptSearch, promptResult)
				.enterDailyQuantity(EIGHT_HOURS)
				.submitTimeOff();
	}
}