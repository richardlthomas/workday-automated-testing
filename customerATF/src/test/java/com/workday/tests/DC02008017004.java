package com.workday.tests;

import static com.workday.testengineering.html.core.HtmlPageExpressions.loaded;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.inject.Inject;

import org.testng.annotations.AfterMethod;

import com.workday.testengineering.browser.Browser;
import com.workday.testengineering.html.component.CurrentUser;
import com.workday.testengineering.html.pages.LoginPage;
import com.workday.testengineering.html.pages.LotusLandingPage;
import com.workday.testengineering.shared.polling.PublishingPollingAssistant;
import com.workday.testengineering.shared.testbase.InjectedReportingExpressiveTest;
import com.workday.testengineering.webdriver.WebTraffic;
import com.workday.testengineering.webdriver.WorkdayWebDriver;

public class DC02008017004 extends InjectedReportingExpressiveTest {

	@Inject private PublishingPollingAssistant pollingAssistant;
	@Inject private LoginPage loginPage;
	@Inject private Browser browser;
	@Inject private WorkdayWebDriver driver;
	@Inject private WebTraffic webTraffic;
	@Inject private LotusLandingPage landingPage;
	
	private static final String CC_MANAGER = "wd_ldap_test01";
	private static final String CC_PASS = "Wdtest0101";
	private static final String COMPANY_LEDGER = "";
	private static final String JOURNAL_SOURCE = "";
	private static final int LEDGER_AMOUNT = 0;
	private static final int DEBIT_AMOUNT = 0;
	private static final String MEMO = "";
	private static final String[] WORKTAGS = {"", ""};
	
	@AfterMethod(alwaysRun=true)
	public void signOut(){
		//safelySignOut();
		//stopDrivers();
	}
	
	@Test (groups = { "exampleTest" })
	public void createJournalEntry() {
		logInAs(CC_MANAGER, CC_PASS);
	}
	
	private void logInAs(String user, String pass) {
		loginPage.logIn(user, pass);
		waitUntil(landingPage, is(loaded()));
	}
	
	public void safelySignOut() {
		CurrentUser currentUser = new CurrentUser(driver, pollingAssistant);
		if (currentUser.isLoggedIn())
			currentUser.signOut();
	}
	
	public void stopDrivers() {
		webTraffic.writeHarTo(getClass().getName());
		webTraffic.stop();
		if (driver != null)
			driver.quit();
	}
	
}
