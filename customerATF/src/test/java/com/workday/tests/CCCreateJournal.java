package com.workday.tests;

//import static com.workday.testengineering.html.core.HtmlPageExpressions.loaded;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

//import javax.inject.Inject;







import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.workday.testengineering.html.component.ScopedSearch;

//import com.workday.testengineering.webdriver.WorkdayWebDriver;

public class CCCreateJournal {

//	public static WebDriver chromeDriver;
	public static WebDriver firefoxDriver;
//	public static WebDriver iexploreDriver;
//	@Inject private WorkdayWebDriver workdayDriver;
	public final static String URL = "https://wd5-impl.workday.com/Rochester2/login.flex?redirect=n";
	public static WebDriverWait wait;
	private static Calendar calendar;
	
	private static final String CC_MANAGER = "wd_ldap_test01";
	private static final String CC_PASS = "Wdtest0101";
	private static final String COMPANY_LEDGER = "021";
	private static String dateString;
	private static final String JOURNAL_SOURCE = "Manual";
	private static final int LEDGER_AMOUNT = 0;
	private static final int DEBIT_AMOUNT = 0;
	private static final String MEMO_LINE = "";
	private static final String[] JOURNAL_WORKTAGS = {"", ""};
	
	private static WebElement usernameField;
	private static WebElement passwordField;
//	private static WebElement loginButton;
	private static WebElement searchBox;
	private static WebElement createJournalLink;
	private static WebElement ledgerBox;
	private static WebElement journalSourceBox;
	private static WebElement acctDateBox;
	private static WebElement wdAppFrame;
	private static WebElement searchResults;
//	private static WebElement searchPopup;
	private static ScopedSearch searchPopup;
//	private static WebElement manualOption;
	private static WebElement continueButton;
	private static List<WebElement> promptInputs;
	private static List<WebElement> promptOptions;
	
	@BeforeMethod(alwaysRun = true)
	public void initialize() throws InterruptedException{
//		System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
//		chromeDriver = new ChromeDriver();
//		chromeDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		wait = new WebDriverWait(chromeDriver, 10);
//		TestNGThread.sleep(5000);
		firefoxDriver = new FirefoxDriver();
		firefoxDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		wait = new WebDriverWait(firefoxDriver, 10);
//		System.setProperty("webdriver.ie.driver", "C:\\Program Files (x86)\\Internet Explorer\\iexplore.exe");
//		iexploreDriver = new InternetExplorerDriver();
//		iexploreDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		wait = new WebDriverWait(iexploreDriver, 10);
//		workdayDriver = new WorkdayWebDriver();
//		iexploreDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		wait = new WebDriverWait(iexploreDriver, 10);
		System.out.println("URL loaded");
	}
	
	@Test (groups = { "exampleTest" })
	public void createJournalEntry() throws InterruptedException {
		logInAs(CC_MANAGER, CC_PASS);
		searchFor("creat jour");
		selectCreateJournal();
		createJournal(COMPANY_LEDGER, JOURNAL_SOURCE, LEDGER_AMOUNT, DEBIT_AMOUNT, MEMO_LINE, JOURNAL_WORKTAGS);
	}
	
	private void selectCreateJournal() {
		searchResults = findElement("id", "wd-SearchResult");
		System.out.println("Found searchResults: " + searchResults.toString());
		promptOptions = searchResults.findElements(By.cssSelector("div[automationid=promptOption]"));
		System.out.println("search results: " + promptOptions.toString());
		createJournalLink = promptOptions.get(0);
		System.out.println("Found create journal link: " + createJournalLink.toString());
		createJournalLink.click();
	}

	private void createJournal(String companyLedger, String journalSource,
			int ledgerAmount, int debitAmount, String memo, String[] worktags) throws InterruptedException {
		wdAppFrame = findElement("id", "workdayApplicationFrame");
		promptInputs = wdAppFrame.findElements(By.cssSelector("input[automationid=promptInput]"));
		System.out.println("journal inputs: " + promptInputs.toString());
		ledgerBox = promptInputs.get(0);
		System.out.println("Found ledger box: " + ledgerBox.toString());
		ledgerBox.sendKeys(companyLedger);
		ledgerBox.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		dateString = getTodaysDate();
		acctDateBox = findElement("cssSelector", "input[automationid=inputDateBox]");
		acctDateBox.sendKeys(dateString);
		journalSourceBox = promptInputs.get(1);
		journalSourceBox.sendKeys(journalSource);
		journalSourceBox.sendKeys(Keys.ENTER);
		Thread.sleep(500);
		searchPopup = null;
		searchPopup.selectSearchResult(journalSource);
//		searchPopup = findElement("cssSelector", "input[automationid=searchBox]");
//		promptOptions = searchPopup.findElements(By.cssSelector("div[automationid=promptOption]"));
//		manualOption = promptOptions.get(0);
//		manualOption.click();
		continueButton = findElement("cssSelector", "a[automationid=wd-CommandButton_PRIMARY]");
		continueButton.click();
	}

	private String getTodaysDate() {
		calendar = Calendar.getInstance();
		String theDate;
		String month = Integer.toString(calendar.get(Calendar.MONTH)+1);
		String day = Integer.toString(calendar.get(Calendar.DATE));
		String year = Integer.toString(calendar.get(Calendar.YEAR));
		if(Calendar.MONTH<9){
			month = "0"+month;
		}
		if(Calendar.DATE<10){
			day = "0"+day;
		}
		theDate = month+day+year;
		return theDate;
	}

	private void searchFor(String searchTerms) {
		searchBox = findElement("cssSelector", "input[automationid=globalSearchInput]");
		searchBox.sendKeys(searchTerms);
		searchBox.sendKeys(Keys.RETURN);
	}

	private void logInAs(String user, String pass) {
//		chromeDriver.get(URL);
		firefoxDriver.get(URL);
//		iexploreDriver.get(URL);
		usernameField = findElement("cssSelector", "input[automationid=userName]");
		System.out.println("Found username field: " + usernameField.toString());
		passwordField = findElement("cssSelector", "input[automationid=password]");
		System.out.println("Found password field: " + passwordField.toString());
//		loginButton = findElement("cssSelector", "input[automationid=goButton]");
//		System.out.println("Found login button: " + loginButton.toString());
		usernameField.sendKeys(user);
		passwordField.sendKeys(pass);
		passwordField.sendKeys(Keys.ENTER);
//		loginButton.click();
		//waitUntil(landingPage, is(loaded()));
	}
	
	public static WebElement findElement(String byType, String selector){
		switch (byType){
		case "className":
			return wait.until(ExpectedConditions.elementToBeClickable(By.className(selector)));
		case "cssSelector":
			return wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(selector)));
		case "id":
			return wait.until(ExpectedConditions.elementToBeClickable(By.id(selector)));
		case "linkText":
			return wait.until(ExpectedConditions.elementToBeClickable(By.linkText(selector)));
		case "name":
			return wait.until(ExpectedConditions.elementToBeClickable(By.name(selector)));
		case "partialLinkText":
			return wait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText(selector)));
		case "tagName":
			return wait.until(ExpectedConditions.elementToBeClickable(By.tagName(selector)));
		case "xpath":
			return wait.until(ExpectedConditions.elementToBeClickable(By.xpath(selector)));
		}
		return wait.until(ExpectedConditions.elementToBeClickable(By.className("headerImgBOT")));
	}
	
	@AfterMethod(alwaysRun=true)
	public void signOut(){
//		chromeDriver.quit();
		firefoxDriver.quit();
//		iexploreDriver.quit();
	}
	
}
