package com.workday.tests;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NonPOSupInv {

//	public static WebDriver chromeDriver;
	public static WebDriver firefoxDriver;
//	public static WebDriver iexploreDriver;
//	@Inject private WorkdayWebDriver workdayDriver;
	public final static String URL = "https://wd5-impl.workday.com/Rochester2/login.flex?redirect=n";
	public static WebDriverWait wait;
	
	private static final String DATA_ENTRY_SPEC = "wd_ldap_test01";
	private static final String DES_PASS = "Wdtest0101";
	private static final String CRE_SUP_INV = "cre sup inv";
	private static final String COMPANY = "020";
	private static final String SUPPLIER = "Bagel Land";
	private static final String AMOUNT = "1000.00";
	private static final String SPEND_CATEGORY = "SC49650";
	private static final String INVOICE_WORKTAG = "LN082205";
//	private static final String[] INVOICE_WORKTAGS = {"SC49650"};
	
	private static WebElement usernameField;
	private static WebElement passwordField;
//	private static WebElement loginButton;
	private static WebElement searchBox;
	private static WebElement createInvoiceLink;
	private static WebElement companyBox;
	private static WebElement supplierBox;
	private static WebElement tableRow;
	private static WebElement spendCategoryCell;
	private static WebElement spendCategoryBox;
	private static WebElement amountCell;
	private static WebElement amountBox;
	private static WebElement worktagCell;
	private static WebElement worktagBox;
	private static WebElement wdAppFrame;
	private static WebElement searchResults;
	private static WebElement continueButton;
	private static WebElement doneButton;
	private static List<WebElement> promptInputs;
	private static List<WebElement> promptOptions;
	private static List<WebElement> tableCells;
 	
	@BeforeMethod(alwaysRun = true)
	public void initialize() throws InterruptedException{
//		System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
//		chromeDriver = new ChromeDriver();
//		chromeDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		wait = new WebDriverWait(chromeDriver, 10);
//		TestNGThread.sleep(5000);
		firefoxDriver = new FirefoxDriver();
//		firefoxDriver.manage().window().maximize();
		firefoxDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		wait = new WebDriverWait(firefoxDriver, 15);
//		System.setProperty("webdriver.ie.driver", "C:\\Program Files (x86)\\Internet Explorer\\iexplore.exe");
//		iexploreDriver = new InternetExplorerDriver();
//		iexploreDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		wait = new WebDriverWait(iexploreDriver, 10);
////		workdayDriver = new WorkdayWebDriver();
//		iexploreDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		wait = new WebDriverWait(iexploreDriver, 10);
		System.out.println("URL loaded");
	}
	
	@Test (groups = { "exampleTest" })
	public void createInvoice() throws InterruptedException {
		logInAs(DATA_ENTRY_SPEC, DES_PASS);
		searchFor(CRE_SUP_INV);
		selectCreateInvoice();
		createSupInv(COMPANY, SUPPLIER, AMOUNT, SPEND_CATEGORY, INVOICE_WORKTAG);
	}
	
	private void logInAs(String user, String pass) {
//		chromeDriver.get(URL);
		firefoxDriver.get(URL);
//		iexploreDriver.get(URL);
		usernameField = findElement("cssSelector", "input[automationid=userName]");
		System.out.println("Found username field: " + usernameField.toString());
		passwordField = findElement("cssSelector", "input[automationid=password]");
		System.out.println("Found password field: " + passwordField.toString());
//		loginButton = findElement("cssSelector", "input[automationid=goButton]");
//		System.out.println("Found login button: " + loginButton.toString());
		usernameField.sendKeys(user);
		passwordField.sendKeys(pass);
		passwordField.sendKeys(Keys.ENTER);
//		loginButton.click();
		//waitUntil(landingPage, is(loaded()));
	}
	
	private void searchFor(String searchTerms) {
		searchBox = findElement("cssSelector", "input[automationid=globalSearchInput]");
		searchBox.clear();
		searchBox.sendKeys(searchTerms);
		searchBox.sendKeys(Keys.RETURN);
	}
	
	private void selectCreateInvoice() {
		searchResults = findElement("id", "wd-SearchResult");
		System.out.println("Found searchResults: " + searchResults.toString());
		promptOptions = searchResults.findElements(By.cssSelector("div[automationid=promptOption]"));
		System.out.println("search results: " + promptOptions.toString());
		createInvoiceLink = promptOptions.get(1);
		System.out.println("Found create invoice link: " + createInvoiceLink.toString());
		createInvoiceLink.click();
	}
	
	private void createSupInv(String company, String supplier, String amount, String spendCategory, String worktags) throws InterruptedException {
		wdAppFrame = findElement("id", "workdayApplicationFrame");
		promptInputs = wdAppFrame.findElements(By.cssSelector("input[automationid=promptInput]"));
		System.out.println("number of prompt inputs: " + promptInputs.size());
		System.out.println("journal inputs: " + promptInputs.toString());
		companyBox = promptInputs.get(0);
		System.out.println("Found ledger box: " + companyBox.toString());
		companyBox.sendKeys(company);
		companyBox.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		supplierBox = promptInputs.get(1);
		supplierBox.sendKeys(supplier);
		supplierBox.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		tableRow = wdAppFrame.findElement(By.className("GHI30FIDEKH"));
		System.out.println("found tableRow: " + tableRow.toString());
		System.out.println("table row class: " + tableRow.getAttribute("class"));
		tableRow.click();
		Thread.sleep(500);
		tableRow.sendKeys(amount);
		tableRow.click();
		tableCells = tableRow.findElements(By.cssSelector("td[automationid=gridCell]"));
		System.out.println("found tableCells: " + tableCells.toString());
		System.out.println("number of row cells: " + tableCells.size());
		System.out.println("row cells: " + tableCells.toString());
		System.out.println("amountCell = " + tableCells.get(5).toString());
		spendCategoryCell = tableCells.get(5);
		spendCategoryCell.click();
		spendCategoryBox = spendCategoryCell.findElement(By.tagName("input"));
		spendCategoryBox.click();
		spendCategoryBox.sendKeys(spendCategory);
		spendCategoryBox.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		amountCell = tableCells.get(9);
		amountCell.click();
		amountBox = amountCell.findElement(By.tagName("input"));
		System.out.println("Found amount box: " + amountBox.toString());
		amountBox.click();
		amountBox.clear();
		amountBox.sendKeys(Keys.BACK_SPACE);
		amountBox.sendKeys(Keys.BACK_SPACE);
		amountBox.sendKeys(Keys.BACK_SPACE);
		amountBox.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(500);
		amountBox.sendKeys(AMOUNT);
		worktagCell = tableCells.get(11);
		worktagBox = worktagCell.findElement(By.tagName("input"));
		System.out.println("found worktagCell: " + worktagCell.toString());
		System.out.println("found worktagBox: " + worktagBox.toString());
		WebElement worktagDiv = worktagCell.findElement(By.tagName("div"));
		worktagDiv.click();
		worktagBox.click();
		worktagBox.clear();
		Thread.sleep(1000);
		worktagBox.sendKeys(worktags);
		worktagBox.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		continueButton = findElement("cssSelector", "a[automationid=wd-CommandButton_PRIMARY]");
		continueButton.click();
		Thread.sleep(3000);
		doneButton = findElement("cssSelector", "a[automationid=wd-CommandButton_SECONDARY_Done_Button]");
		doneButton.click();
		Thread.sleep(3000);
	}
	
	public static WebElement findElement(String byType, String selector){
		switch (byType){
		case "className":
			return wait.until(ExpectedConditions.elementToBeClickable(By.className(selector)));
		case "cssSelector":
			return wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(selector)));
		case "id":
			return wait.until(ExpectedConditions.elementToBeClickable(By.id(selector)));
		case "linkText":
			return wait.until(ExpectedConditions.elementToBeClickable(By.linkText(selector)));
		case "name":
			return wait.until(ExpectedConditions.elementToBeClickable(By.name(selector)));
		case "partialLinkText":
			return wait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText(selector)));
		case "tagName":
			return wait.until(ExpectedConditions.elementToBeClickable(By.tagName(selector)));
		case "xpath":
			return wait.until(ExpectedConditions.elementToBeClickable(By.xpath(selector)));
		}
		return wait.until(ExpectedConditions.elementToBeClickable(By.className("headerImgBOT")));
	}
	
	@AfterMethod(alwaysRun=true)
	public void signOut(){
//		chromeDriver.quit();
		firefoxDriver.quit();
//		iexploreDriver.quit();
	}
	
}
