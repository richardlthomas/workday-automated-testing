package com.workday.tests;

import static com.workday.testengineering.html.core.HtmlPageExpressions.loaded;

import javax.inject.Inject;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.workday.html.tasks.ChangeContactInformationTask;
import com.workday.testbase.LotusTest;
import com.workday.testengineering.html.pages.LotusLandingPage;

public class ChangeContactInformationTests extends LotusTest {

	private static final String NEW_ADDRESS = "1708 Broderick St";
	private static final String AN_EMPLOYEE = "lmcneil";
	
	@Inject private ChangeContactInformationTask changeContactInformationTask;
	@Inject private LotusLandingPage landingPage;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//The After Method tears down the test, logging out the user. 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@AfterMethod(alwaysRun=true)
	public void signOut() {
		safelySignOut();	
	}	
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//The test changes the address on the logged in user profile. (An orchestration page)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test (groups = { "exampleTest" })
	public void changeAddress() {
		logInAs(AN_EMPLOYEE);
		navigateToChangeContactInfoPage();
		changePrimaryStreetAddress();
		verifyChangeInAddress();
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Test helper methods.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	private void logInAs(String username) {
		loginAs(username);
		waitUntil(landingPage, is(loaded()));
	}

	private void navigateToChangeContactInfoPage() {
		changeContactInformationTask
			.initiateFromCurrentUserProfile();
	}
	
	private void changePrimaryStreetAddress() {
		changeContactInformationTask
			.onChangeContactInformationOrchestrationViewPage()
				.editPrimaryAddress()
				.openPrimaryAddressCompositView()
				.enterAddressLineOne(NEW_ADDRESS)
				.closePrimaryAddressCompositView()
				.submitChanges();
	}
	
	private void verifyChangeInAddress() {
		changeContactInformationTask
			.onChangeContactInformationConfirmationPage();
	}
}