package com.workday.tests;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CreateAdHocTrans {

//	public static WebDriver chromeDriver;
	public static WebDriver firefoxDriver;
//	public static WebDriver iexploreDriver;
//	@Inject private WorkdayWebDriver workdayDriver;
	public final static String URL = "https://wd5-impl.workday.com/Rochester2/login.flex?redirect=n";
	public static WebDriverWait wait;
	
	private static final String CASH_ANALYST = "wd_ldap_test01";
	private static final String CA_PASS = "Wdtest0101";
	private static final String CRE_AD_HOC = "cre ad hoc";
	private static final String COMPANY = "020";
	private static final String TEMPLATE = "TC2testing";
	private static final String ACCOUNT = "AMEX-BIP Account";
	private static final String AMOUNT = "1000.00";
	private static final String REFERENCE = "test reference";
	private static final boolean IS_DEPOSIT = true;
	
	private static WebElement usernameField;
	private static WebElement passwordField;
//	private static WebElement loginButton;
	private static WebElement searchBox;
	private static WebElement createAdHocLink;
	private static WebElement companyBox;
	private static WebElement templateBox;
	private static WebElement accountBox;
	private static WebElement transAmntBox;
	private static WebElement okButton;
	private static WebElement amntRow;
	private static WebElement amountCell;
	private static WebElement amountBox;
	private static WebElement referenceBox;
	private static WebElement radioSection;
//	private static WebElement depositSpan;
//	private static WebElement withdrawalSpan;
	private static WebElement depositRadio;
	private static WebElement withdrawalRadio;
	private static WebElement wdAppFrame;
	private static WebElement searchResults;
	private static WebElement submitButton;
	private static WebElement tableRow;
	private static List<WebElement> promptInputs;
	private static List<WebElement> promptOptions;
	private static List<WebElement> textInputs;
	private static List<WebElement> tableCells;
	private static List<WebElement> radioButtons;
	
	@BeforeMethod(alwaysRun = true)
	public void initialize() throws InterruptedException{
//		System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
//		chromeDriver = new ChromeDriver();
//		chromeDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		wait = new WebDriverWait(chromeDriver, 10);
//		TestNGThread.sleep(5000);
		firefoxDriver = new FirefoxDriver();
//		firefoxDriver.manage().window().maximize();
		firefoxDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		wait = new WebDriverWait(firefoxDriver, 15);
//		System.setProperty("webdriver.ie.driver", "C:\\Program Files (x86)\\Internet Explorer\\iexplore.exe");
//		iexploreDriver = new InternetExplorerDriver();
//		iexploreDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		wait = new WebDriverWait(iexploreDriver, 10);
//		workdayDriver = new WorkdayWebDriver();
//		iexploreDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		wait = new WebDriverWait(iexploreDriver, 10);
		System.out.println("URL loaded");
	}
	
	@Test (groups = { "exampleTest" })
	public void createAdHocTrans() throws InterruptedException {
		logInAs(CASH_ANALYST, CA_PASS);
		searchFor(CRE_AD_HOC);
		selectCreateAdHoc();
		setAdHocTemplate(COMPANY, TEMPLATE, ACCOUNT);
		createAdHoc(AMOUNT, IS_DEPOSIT);
	}
	
	private void logInAs(String user, String pass) {
//		chromeDriver.get(URL);
		firefoxDriver.get(URL);
//		iexploreDriver.get(URL);
		usernameField = findElement("cssSelector", "input[automationid=userName]");
		System.out.println("Found username field: " + usernameField.toString());
		passwordField = findElement("cssSelector", "input[automationid=password]");
		System.out.println("Found password field: " + passwordField.toString());
//		loginButton = findElement("cssSelector", "input[automationid=goButton]");
//		System.out.println("Found login button: " + loginButton.toString());
		usernameField.sendKeys(user);
		passwordField.sendKeys(pass);
		passwordField.sendKeys(Keys.ENTER);
//		loginButton.click();
		//waitUntil(landingPage, is(loaded()));
	}
	
	private void selectCreateAdHoc() {
		searchResults = findElement("id", "wd-SearchResult");
		System.out.println("Found searchResults: " + searchResults.toString());
		promptOptions = searchResults.findElements(By.cssSelector("div[automationid=promptOption]"));
		System.out.println("search results: " + promptOptions.toString());
		createAdHocLink = promptOptions.get(2);
		System.out.println("Found create ad hoc link: " + createAdHocLink.toString());
		createAdHocLink.click();
	}
	
	private void setAdHocTemplate(String company, String template, String account) throws InterruptedException{
		wdAppFrame = findElement("id", "workdayApplicationFrame");
		promptInputs = wdAppFrame.findElements(By.cssSelector("input[automationid=promptInput]"));
		companyBox = promptInputs.get(0);
		companyBox.sendKeys(company);
		companyBox.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		templateBox = promptInputs.get(1);
		templateBox.sendKeys(template);
		templateBox.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		accountBox = promptInputs.get(2);
		accountBox.sendKeys(account);
		accountBox.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		okButton = findElement("cssSelector", "a[automationid=wd-CommandButton_SECONDARY__SUBMIT_TaskId_Edit_Page]");
		System.out.println("Found ok button: " + okButton.toString());
		okButton.click();
		Thread.sleep(3000);
	}
	
	private void createAdHoc(String amount, boolean isDeposit) throws InterruptedException{
		wdAppFrame = findElement("id", "workdayApplicationFrame");
		textInputs = wdAppFrame.findElements(By.cssSelector("input[type=text]"));
		System.out.println("number of text boxes: " + textInputs.size());
		System.out.println("text boxes: " + textInputs.toString());
		Thread.sleep(1000);
		amntRow = findElement("id", "wd-Currency-56$240666");
		transAmntBox = amntRow.findElement(By.tagName("input"));
		Thread.sleep(1000);
		System.out.println("Found amount box: " + transAmntBox.getText() + " " + searchResults.toString());
		transAmntBox.clear();
		transAmntBox.sendKeys(Keys.BACK_SPACE);
		transAmntBox.sendKeys(Keys.BACK_SPACE);
		transAmntBox.sendKeys(Keys.BACK_SPACE);
		transAmntBox.sendKeys(Keys.BACK_SPACE);
		transAmntBox.sendKeys(amount);
		referenceBox = wdAppFrame.findElement(By.id("wd-TextInput-56$240670"));
		referenceBox.sendKeys(REFERENCE);
		radioSection = wdAppFrame.findElement(By.tagName("fieldset"));
		System.out.println("Found radio section: " + radioSection.toString());
		radioButtons = radioSection.findElements(By.tagName("input"));
//		depositSpan = wdAppFrame.findElement(By.cssSelector("span[automationid=56$240667]"));
//		depositRadio = depositSpan.findElement(By.tagName("input"));
		depositRadio = radioButtons.get(0);
//		withdrawalSpan = wdAppFrame.findElement(By.cssSelector("span[automationid=56$240668]"));
//		withdrawalRadio = withdrawalSpan.findElement(By.tagName("input"));
		withdrawalRadio = radioButtons.get(1);
		if(isDeposit){
			depositRadio.click();
		}
		else{
			withdrawalRadio.click();
		}
		wdAppFrame.findElement(By.id("0-wd-Currency-56$240692")).click();
		tableRow = wdAppFrame.findElement(By.className("GHI30FIDCXE"));
		System.out.println("found tableRow: " + tableRow.toString());
		System.out.println("table row class: " + tableRow.getAttribute("class"));
		tableRow.click();
		Thread.sleep(500);
		tableRow.sendKeys(amount);
		tableCells = tableRow.findElements(By.cssSelector("td[automationid=gridCell]"));
		System.out.println("found tableCells: " + tableCells.toString());
		System.out.println("number of row cells: " + tableCells.size());
		System.out.println("row cells: " + tableCells.toString());
		System.out.println("amountCell = " + tableCells.get(5).toString());
		amountCell = tableCells.get(5);
		amountBox = amountCell.findElement(By.tagName("input"));
		System.out.println("Found amount box: " + amountBox.toString());
		amountBox.sendKeys(Keys.BACK_SPACE);
		amountBox.sendKeys(Keys.BACK_SPACE);
		amountBox.sendKeys(Keys.BACK_SPACE);
		amountBox.sendKeys(Keys.BACK_SPACE);
		amountBox.sendKeys(AMOUNT);
		submitButton = findElement("cssSelector", "a[automationid=wd-CommandButton_PRIMARY]");
		submitButton.click();
	}
	
	private void searchFor(String searchTerms) {
		searchBox = findElement("cssSelector", "input[automationid=globalSearchInput]");
		searchBox.clear();
		searchBox.sendKeys(searchTerms);
		searchBox.sendKeys(Keys.RETURN);
	}
	
	public static WebElement findElement(String byType, String selector){
		switch (byType){
		case "className":
			return wait.until(ExpectedConditions.elementToBeClickable(By.className(selector)));
		case "cssSelector":
			return wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(selector)));
		case "id":
			return wait.until(ExpectedConditions.elementToBeClickable(By.id(selector)));
		case "linkText":
			return wait.until(ExpectedConditions.elementToBeClickable(By.linkText(selector)));
		case "name":
			return wait.until(ExpectedConditions.elementToBeClickable(By.name(selector)));
		case "partialLinkText":
			return wait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText(selector)));
		case "tagName":
			return wait.until(ExpectedConditions.elementToBeClickable(By.tagName(selector)));
		case "xpath":
			return wait.until(ExpectedConditions.elementToBeClickable(By.xpath(selector)));
		}
		return wait.until(ExpectedConditions.elementToBeClickable(By.className("headerImgBOT")));
	}
	
	@AfterMethod(alwaysRun=true)
	public void signOut(){
//		chromeDriver.quit();
//		firefoxDriver.quit();
//		iexploreDriver.quit();
	}
	
}
