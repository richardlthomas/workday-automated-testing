package com.workday.testbase;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.time.DateUtils;
import org.openqa.selenium.Cookie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.workday.testengineering.browser.Browser;
import com.workday.testengineering.flex.FlexModule;
import com.workday.testengineering.html.component.CurrentUser;
import com.workday.testengineering.html.pages.LoginPage;
import com.workday.testengineering.shared.injecting.Modules;
import com.workday.testengineering.shared.polling.PublishingPollingAssistant;
import com.workday.testengineering.shared.testbase.InjectedReportingExpressiveTest;
import com.workday.testengineering.webdriver.WebTraffic;
import com.workday.testengineering.webdriver.WorkdayWebDriver;
import com.workday.testengineering.webdriver.inject.WebDriverModule;

@Modules({ WebDriverModule.class, FlexModule.class })
public class LotusTest extends InjectedReportingExpressiveTest {
	protected final Logger log = LoggerFactory.getLogger(getClass());

	@Inject @Named("browser.host") private String host;

	@Inject private WebTraffic webTraffic;
	@Inject private WorkdayWebDriver driver;
	@Inject private PublishingPollingAssistant pollingAssistant;
	@Inject private LoginPage loginPage;
	@Inject private Browser browser;

	@BeforeClass(alwaysRun = true)
	public void beforeLogging() {
		webTraffic.newHar(getClass().getName());
		enableFlexAutomation();
	}

	@AfterClass(alwaysRun = true)
	public void stopDrivers() {
		webTraffic.writeHarTo(getClass().getName());
		webTraffic.stop();
		if (driver != null)
			driver.quit();
	}

	public LoginPage navigateToLogin() {
		return loginPage.go();
	}

	public void loginAs(String user) {
		navigateToLogin().logInAs(user).submitOnly();
	}

	public void safelySignOut() {
		CurrentUser currentUser = new CurrentUser(driver, pollingAssistant);
		if (currentUser.isLoggedIn())
			currentUser.signOut();
	}

	private void enableFlexAutomation() {
		navigateToLogin();
		Date now = new Date();
		Date expirationDate = DateUtils.addDays(now, 1);
		Cookie automationCookie = new Cookie("enableAutomation", "true", "/", expirationDate);
		browser.addCookie(automationCookie);
	}

	protected boolean isSauceJob() {
		return host.equals("sauce");
	}
}