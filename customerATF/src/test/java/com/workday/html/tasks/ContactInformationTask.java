package com.workday.html.tasks;

import javax.inject.Inject;

import org.openqa.selenium.WebDriver;

import com.workday.html.pages.edit.ContactInformationStartPage;
import com.workday.html.pages.view.ContactInformationPage;
import com.workday.testengineering.html.tasks.Task;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class ContactInformationTask extends Task {

	private ContactInformationPage contactInformationPage;
	private ContactInformationStartPage contactInformationStartPage;

	@Inject
	public ContactInformationTask(WebDriver driver, PollingAssistant assistant, ContactInformationPage contactInformationPage, 
			ContactInformationStartPage contactInformationStartPage) {
		super(driver, assistant);
		this.contactInformationPage = contactInformationPage;
		this.contactInformationStartPage = contactInformationStartPage;
	}

	public ContactInformationPage onContactInformationPage() {
		return onPage(contactInformationPage);
	}

	public ContactInformationStartPage onContactInformationStartPage() {
		return onPage(contactInformationStartPage);
	}
}