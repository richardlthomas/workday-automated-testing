package com.workday.html.tasks;

import javax.inject.Inject;

import org.openqa.selenium.WebDriver;

import com.workday.html.pages.confirmation.PositionCreateConfirmationPage;
import com.workday.html.pages.edit.ApproveCreatePositionEditPage;
import com.workday.testengineering.html.tasks.Task;
import com.workday.testengineering.shared.polling.PollingAssistant;
import com.workday.testengineering.shared.suv.Tenant;

public class ApprovePositionTask extends Task {
	
	private PositionCreateConfirmationPage positionCreateConfirmationPage;
	private ApproveCreatePositionEditPage approveCreatePositionEditPage;
	
	
	@Inject
	public ApprovePositionTask(WebDriver driver, PollingAssistant assistant, 
			PositionCreateConfirmationPage positionCreateConfirmationPage, ApproveCreatePositionEditPage approveCreatePositionEditPage, Tenant tenant){
		super(driver, assistant, tenant);
		this.positionCreateConfirmationPage = positionCreateConfirmationPage;
		this.approveCreatePositionEditPage = approveCreatePositionEditPage;
	}
	
	public PositionCreateConfirmationPage onPositionCreateConfirmationPage() {
		return onPage(positionCreateConfirmationPage);
	}

	public ApproveCreatePositionEditPage onApproveCreatePositionEditPage() {
		return onPage(approveCreatePositionEditPage);
	}

	public ApprovePositionTask initiateFromInbox(String title, int index) {
		navigateByInbox().selectActionItem(title, index).run();
		return this;
	}
}
