package com.workday.html.tasks;

import static com.workday.testengineering.html.navigation.Navigations.navigateBy;

import javax.inject.Inject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.workday.html.pages.edit.EmployeeRequestAdhocCompensationChangeStartPage;
import com.workday.html.pages.edit.RequestCompensationChangeForOrchestrationView;
import com.workday.testengineering.html.pages.LotusConfirmationPage;
import com.workday.testengineering.html.tasks.Task;
import com.workday.testengineering.html.utilities.TestUtil;
import com.workday.testengineering.shared.polling.PollingAssistant;
import com.workday.testengineering.shared.suv.Tenant;

public class RequestCompensationChangeTask extends Task {

	private static final String CONFIRM_PAGE = "wd-ConfirmPage-56$79586";
	private static final String COMPENSATION = "Compensation";
	private static final String REQUEST_COMPENSATION_CHANGE = "Request Compensation Change";

	private final EmployeeRequestAdhocCompensationChangeStartPage employeeRequestAdhocCompensationChangeStartPage;
	private final RequestCompensationChangeForOrchestrationView requestCompensationChangeForOrchestrationView;

	@Inject
	public RequestCompensationChangeTask(WebDriver driver, PollingAssistant assistant, Tenant tenant, EmployeeRequestAdhocCompensationChangeStartPage employeeRequestAdhocCompensationChangeStartPage, RequestCompensationChangeForOrchestrationView requestCompensationChangeForOrchestrationView) {
		super(driver, assistant, tenant);
		this.employeeRequestAdhocCompensationChangeStartPage = employeeRequestAdhocCompensationChangeStartPage;
		this.requestCompensationChangeForOrchestrationView = requestCompensationChangeForOrchestrationView;
	}

	public RequestCompensationChangeTask initiateFromProfileRelatedAction(By root, String instanceId, String text) {
		navigateBy(driver()).open(profile(instanceId)).run();
		TestUtil.pause();
		navigateBy(relatedAction()).openAndSelect(element(root), text, COMPENSATION, REQUEST_COMPENSATION_CHANGE).run();
		return this;
	}
	
	public EmployeeRequestAdhocCompensationChangeStartPage onEmployeeRequestAdhocCompensationChangeStartPage(){
		return onPage(employeeRequestAdhocCompensationChangeStartPage);
	}

	public RequestCompensationChangeForOrchestrationView onRequestCompensationChangeForOrchestrationViewPage(){
		return onPage(requestCompensationChangeForOrchestrationView);
	}

	public LotusConfirmationPage onRequestCompensationChangeConfirmationPage() {
		return onPage(new LotusConfirmationPage(driver(), pollingAssistant(), CONFIRM_PAGE));
	}
}