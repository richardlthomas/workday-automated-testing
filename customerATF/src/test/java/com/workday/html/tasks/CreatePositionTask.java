package com.workday.html.tasks;

import static com.workday.testengineering.html.navigation.Navigations.navigateBy;

import javax.inject.Inject;

import org.openqa.selenium.WebDriver;

import com.workday.html.pages.confirmation.PositionCreateConfirmationPage;
import com.workday.html.pages.edit.PositionCreatePage;
import com.workday.html.pages.edit.PositionCreateStartPage;
import com.workday.testengineering.html.tasks.Task;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class CreatePositionTask extends Task {
	private final PositionCreateStartPage positionCreateStartPage;
	private final PositionCreatePage positionCreatePage;
	private PositionCreateConfirmationPage positionCreateConfirmationPage;
	private static final String CREATE_POSITION_SEARCH = "Create Position";
	
	@Inject
	public CreatePositionTask(WebDriver driver, PollingAssistant assistant, 
			PositionCreateStartPage positionCreateStartPage, 
			PositionCreatePage positionCreatePage, 
			PositionCreateConfirmationPage positionCreateConfirmationPage) {
		super(driver, assistant);
		this.positionCreateStartPage = positionCreateStartPage;
		this.positionCreatePage = positionCreatePage;
		this.positionCreateConfirmationPage = positionCreateConfirmationPage;
	}
	
	public CreatePositionTask initiateFromSearch() {
		navigateBy(scopedSearch()).searchAndSelect(CREATE_POSITION_SEARCH).run();
		return this;
	}
	
	public PositionCreateStartPage onPositionCreateStartPage() {
		return onPage(positionCreateStartPage);
	}

	public PositionCreatePage onPositionCreatePage() {
		return onPage(positionCreatePage);
	}
	
	public PositionCreateConfirmationPage onPositionCreateConfirmationPage() {
		return onPage(positionCreateConfirmationPage);
	}
}
