package com.workday.html.tasks;

import static com.workday.testengineering.html.navigation.Navigations.navigateBy;

import javax.inject.Inject;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;

import com.workday.html.pages.confirmation.RequestTimeOffConfirmationPage;
import com.workday.html.pages.edit.RequestTimeOffCalendarPage;
import com.workday.html.pages.edit.RequestTimeOffMicroEditPage;
import com.workday.testengineering.html.tasks.Task;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class RequestTimeOffTask extends Task {	

	private RequestTimeOffConfirmationPage requestTimeOffConfirmationPage;
	private RequestTimeOffCalendarPage requestTimeOffCalendarPage;
	private RequestTimeOffMicroEditPage requestTimeOffMicroEditPage;

	@Inject
	public RequestTimeOffTask(WebDriver driver, PollingAssistant assistant,
			RequestTimeOffConfirmationPage requestTimeOffConfirmationPage,
			RequestTimeOffCalendarPage requestTimeOffCalendarPage,
			RequestTimeOffMicroEditPage requestTimeOffMicroEditPage) {
		super(driver, assistant);
		this.requestTimeOffConfirmationPage = requestTimeOffConfirmationPage;
		this.requestTimeOffMicroEditPage = requestTimeOffMicroEditPage;
		this.requestTimeOffCalendarPage = requestTimeOffCalendarPage;
	}

	public RequestTimeOffTask initiateFromSearch() {
		navigateBy(scopedSearch()).searchAndSelect(REQUEST_TIME_OFF).run();
		return this;
	}
	
	public RequestTimeOffConfirmationPage onRequestTimeOffConfirmationPage() {
		return onPage(requestTimeOffConfirmationPage);
	}
	
	public RequestTimeOffMicroEditPage onRequestTimeOffMicroEditPage() {
		return onPage(requestTimeOffMicroEditPage);
	}

	public RequestTimeOffCalendarPage onRequestTimeOffCalendarPage() {
		try {
			return onPage(requestTimeOffCalendarPage);
		} catch (StaleElementReferenceException e) {
			return onPage(requestTimeOffCalendarPage);
		}
	}
	
	private static final String REQUEST_TIME_OFF = "Request Time Off";
}
