package com.workday.html.tasks;

import static com.workday.testengineering.html.core.HtmlPageExpressions.loaded;

import javax.inject.Inject;

import org.openqa.selenium.WebDriver;

import com.workday.html.pages.edit.ChangeContactInformationOrchestrationViewPage;
import com.workday.html.pages.edit.ChangeContactInformationStartPage;
import com.workday.html.pages.view.ContactInformationPage;
import com.workday.html.pages.view.WorkerProfilePage;
import com.workday.testengineering.html.component.CurrentUser;
import com.workday.testengineering.html.pages.LotusConfirmationPage;
import com.workday.testengineering.html.tasks.Task;
import com.workday.testengineering.html.utilities.TestUtil;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class ChangeContactInformationTask extends Task {

	private static final String CONFIRM_PAGE = "wd-ConfirmPage-56$79586";
	
	private ChangeContactInformationOrchestrationViewPage changeContactInformationOrchestrationViewPage;
	private ChangeContactInformationStartPage changeContactInformationStartPage;
	private ContactInformationTask contactInformationTask;
	private WorkerProfilePage workerProfilePage;
	private CurrentUser currentUser;

	@Inject
	public ChangeContactInformationTask(WebDriver driver, PollingAssistant assistant, WorkerProfilePage workerProfilePage, CurrentUser currentUser, ChangeContactInformationStartPage changeContactInformationStartPage, 
			ChangeContactInformationOrchestrationViewPage changeContactInformationOrchestrationViewPage, ContactInformationTask contactInformationTask) {
		super(driver, assistant);
		this.workerProfilePage = workerProfilePage;
		this.currentUser = currentUser;
		this.changeContactInformationStartPage = changeContactInformationStartPage;
		this.changeContactInformationOrchestrationViewPage = changeContactInformationOrchestrationViewPage;
		this.contactInformationTask = contactInformationTask;
	}

	public ChangeContactInformationTask initiateFromCurrentUserProfile() {
		currentUser.viewProfile();
		waitUntil(workerProfilePage, is(loaded()));
		openEditContactInformation();
		return this;
	}

	private void openEditContactInformation() {
		workerProfilePage
			.contactTab()
				.open();
		
		TestUtil.pause();
		workerProfilePage
			.contactTab()
				.contactTab()
					.edit();
	}

	public ChangeContactInformationStartPage onChangeContactInformationStartPage() {
		return onPage(changeContactInformationStartPage);
	}

	public ChangeContactInformationOrchestrationViewPage onChangeContactInformationOrchestrationViewPage() {
		return onPage(changeContactInformationOrchestrationViewPage);
	}
	
	public ContactInformationPage onContactInformationPage() {
		return onPage(contactInformationTask.onContactInformationPage());
	}
	
	public LotusConfirmationPage onChangeContactInformationConfirmationPage() {
		return onPage(new LotusConfirmationPage(driver(), pollingAssistant(), CONFIRM_PAGE));
	}
}