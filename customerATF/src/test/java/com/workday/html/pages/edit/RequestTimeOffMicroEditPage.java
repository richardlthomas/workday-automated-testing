package com.workday.html.pages.edit;

import static com.workday.testengineering.html.core.HtmlElementExpressions.displayed;
import static com.workday.testengineering.html.core.HtmlElementExpressions.enabled;
import static com.workday.testengineering.html.utilities.TestUtil.escapeSpecialChars;

import javax.inject.Inject;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;

import com.workday.testengineering.html.component.LotusRegularPromptWithSingleSelection;
import com.workday.testengineering.html.core.HtmlElement;
import com.workday.testengineering.html.pages.LotusEditPage;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class RequestTimeOffMicroEditPage extends LotusEditPage {

	@Inject
	public RequestTimeOffMicroEditPage(WebDriver driver, PollingAssistant assistant) {
		super(driver, assistant, REQUEST_TIME_OFF_PAGE_ID);
	}

	public RequestTimeOffMicroEditPage selectRequestType(String promptSearchText, String promptResult) {
		LotusRegularPromptWithSingleSelection typePrompt = regularPromptWithSingleSelection(TYPE_PROMPT);
		typePrompt.open();
		typePrompt.selectByInnerSearch(promptSearchText, promptResult);
		return this;
	}

	public RequestTimeOffMicroEditPage enterDailyQuantity(int quantity) {
		HtmlElement dailyQuantity = element(DAILY_QUANTITY_OF_HOURS);
		fillNumericText(dailyQuantity, "" + quantity);
		when(dailyQuantity, is(enabled())).sendKeys(Keys.RETURN);
		return this;
	}

	public RequestTimeOffMicroEditPage submitTimeOff() {
		submit();
		try {
			waitUntil(element(SUBMIT_BUTTON), not(displayed()));
		} catch (StaleElementReferenceException e) {
			waitUntil(element(SUBMIT_BUTTON), not(displayed()));

		}
		waitUntil(element(SUBMITTED_TIME_OFF_REQUEST_CONFIRMATION), is(displayed()));
		return this;
	}

	private static final By SUBMITTED_TIME_OFF_REQUEST_CONFIRMATION = By.xpath("//div[contains(text(), 'Your changes have been saved.')]");
	private static final String TYPE_PROMPT = "wd-MonikerInput-56$293737";
	private static final By DAILY_QUANTITY_OF_HOURS = By.cssSelector(escapeSpecialChars("#wd-Numeric-56$251224 input"));
	private static final By SUBMIT_BUTTON = By.linkText("Submit");
	private static final String REQUEST_TIME_OFF_PAGE_ID = "wd-EditPage-6$55685";
}
