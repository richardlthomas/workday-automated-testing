package com.workday.html.pages.components.profilesubtabs;

import static com.workday.testengineering.html.core.HtmlElementExpressions.displayed;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.workday.testengineering.html.component.ProfileSubLevelTab;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class ContactTabDetails extends ProfileSubLevelTab {

	private static final String CONTACT_TAB = "Contact";
	private static final By EDIT_BUTTON = by(xpath().with("div").withId("wd-DropDownCommandButton-56$236173").with("*").withText("Edit"));
	
	public ContactTabDetails(WebDriver driver, PollingAssistant pollingAssistant) {
		super(driver, pollingAssistant);
	}
	
	public ContactTabDetails open() {
		return open(CONTACT_TAB, this);
	}
	
	public ContactTabDetails edit() {
		when(element(EDIT_BUTTON), is(displayed())).click();
		return this;
	}
}
