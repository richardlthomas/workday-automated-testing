package com.workday.html.pages.edit;

import static org.hamcrest.Matchers.equalTo;

import javax.inject.Inject;

import org.openqa.selenium.WebDriver;

import com.workday.testengineering.html.component.LotusRegularPromptWithSingleSelection;
import com.workday.testengineering.html.pages.LotusEditPage;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class PositionCreateStartPage extends LotusEditPage {

	private static final String CREATE_POSITION_PAGE_ID = "wd-EditPage-6$3039";
	private static final String ORGANIZATION_PROMPT_ROOT = "wd-MonikerInput-56$9743";

	@Inject
	public PositionCreateStartPage(WebDriver driver, PollingAssistant assistant) {
		super(driver, assistant, CREATE_POSITION_PAGE_ID);
	}

	public PositionCreateStartPage verifySelectedSupervisoryOrganization(String organization) {
		LotusRegularPromptWithSingleSelection supervisoryOrganization = regularPromptWithSingleSelection(ORGANIZATION_PROMPT_ROOT);
		String selected = supervisoryOrganization.getSelectedOption();
		assertThat(selected, is(equalTo(organization)));
		return this;
	}
}
