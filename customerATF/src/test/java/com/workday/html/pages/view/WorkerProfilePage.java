package com.workday.html.pages.view;

import static com.workday.testengineering.html.core.HtmlElementExpressions.displayed;

import javax.inject.Inject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.workday.html.pages.components.profilemaintabs.MainContactTab;
import com.workday.html.pages.components.profilemaintabs.MainPersonalTab;
import com.workday.testengineering.html.pages.ViewPage;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class WorkerProfilePage extends ViewPage {

	@Inject
	public WorkerProfilePage(WebDriver driver, PollingAssistant assistant) {
		super(driver, assistant, VIEW_WORKER_PAGE_ID);
	}
	
	public void openFromRelatedAction(String employee, String mainAction, String subAction) {
		relatedAction().openFor(employee).forMainAction(mainAction).selectSubAction(subAction);
	}
	
	public void openFromRelatedAction(String mainAction, String subAction) {
		when(element(PROFILE_RELATED_ACTION_BUTTON), is(displayed())).click();
		relatedAction().forMainAction(mainAction).selectSubAction(subAction);
	}

	public MainPersonalTab personalTab() {
		return new MainPersonalTab(driver(), pollingAssistant());
	}
	
	public MainContactTab contactTab() {
		return new MainContactTab(driver(), pollingAssistant());
	}
	
	private static final String VIEW_WORKER_PAGE_ID = "wd-LotusProfile-6$25778";
	private static final By PROFILE_RELATED_ACTION_BUTTON = By.xpath("//div[contains(@id, 'wd-LotusProfile')]//img[contains(@automationid, 'RELATED_TASK_charm')]");
}