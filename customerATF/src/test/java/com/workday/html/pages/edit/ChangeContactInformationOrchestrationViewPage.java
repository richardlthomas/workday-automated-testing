package com.workday.html.pages.edit;

import static com.workday.testengineering.html.core.HtmlElementExpressions.displayed;

import javax.inject.Inject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.workday.testengineering.html.component.BPFToolbar;
import com.workday.testengineering.html.component.OrchestrationRow;
import com.workday.testengineering.html.core.HtmlElement;
import com.workday.testengineering.html.pages.LotusEditPage;
import com.workday.testengineering.html.utilities.TestUtil;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class ChangeContactInformationOrchestrationViewPage extends LotusEditPage {

	private static final By ADDRESS_LINE_1 = by(xpath().with("*").withId("wd-TextInput-56$34052-0"));
	private static final String PRIMARY_ADDRESS = "wd-ActiveList-6$44745";
	private static final String PAGE_ID = "wd-ViewPage-15$68934";
	private BPFToolbar bpfToolbar;
	
	@Inject
	public ChangeContactInformationOrchestrationViewPage(WebDriver driver, PollingAssistant assistant, BPFToolbar bpfToolbar) {
		super(driver, assistant, PAGE_ID);
		this.bpfToolbar = bpfToolbar;	
	}
	
	public ChangeContactInformationOrchestrationViewPage editPrimaryAddress() {
		primaryAddressRow().edit();
		return this;
	}

	public ChangeContactInformationOrchestrationViewPage openPrimaryAddressCompositView() {
		primaryAddressRow().openCompositeView();
		return this;
	}
	
	public ChangeContactInformationOrchestrationViewPage enterAddressLineOne(String street) {
		TestUtil.pause();
		HtmlElement input = element(ADDRESS_LINE_1);
		when(input, is(displayed())).clear();
		TestUtil.pause();
		input.sendKeys(street);
		TestUtil.pause();
		return this;
	}
	
	public ChangeContactInformationOrchestrationViewPage closePrimaryAddressCompositView() {
		primaryAddressRow().closeCompositeView();
		return this;
	}
	
	public ChangeContactInformationOrchestrationViewPage submitChanges() {
		bpfToolbar.submit();
		return this;
	}
	
	private OrchestrationRow primaryAddressRow() {
		return orchestrationRow(PRIMARY_ADDRESS);
	}
}