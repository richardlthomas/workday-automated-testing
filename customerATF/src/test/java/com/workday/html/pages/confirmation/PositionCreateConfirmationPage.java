package com.workday.html.pages.confirmation;

import javax.inject.Inject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.workday.testengineering.html.pages.LotusConfirmationPage;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class PositionCreateConfirmationPage extends LotusConfirmationPage {
	
	private static final String DETAILS_AND_PROCESS = "Details and Process";
	private static final String CONFIRM_PAGE_ID = "wd-ConfirmPage-56$79586";
	private static final By AVAILABILITY_DATE = By.id("wd-Text-56$22454");

	@Inject
	public PositionCreateConfirmationPage(WebDriver driver, PollingAssistant assistant) {
		super(driver, assistant, CONFIRM_PAGE_ID);
	}
	
	public PositionCreateConfirmationPage openDetails() {
		expandDetailsFor(DETAILS_AND_PROCESS);
		return this;
	}

	public String availabilityDate() {
		return element(AVAILABILITY_DATE).getText();
	}
}
