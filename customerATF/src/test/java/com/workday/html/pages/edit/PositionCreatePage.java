package com.workday.html.pages.edit;

import static com.workday.testengineering.html.core.HtmlElementExpressions.displayed;
import static com.workday.testengineering.shared.waiting.Timer.shortTime;
import static com.workday.testengineering.shared.waiting.Timer.waitFor;

import javax.inject.Inject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.workday.testengineering.html.component.DateControlFactory.TYPE;
import com.workday.testengineering.html.component.LotusRegularPromptWithMultiSelection;
import com.workday.testengineering.html.component.LotusRegularPromptWithSingleSelection;
import com.workday.testengineering.html.core.HtmlElement;
import com.workday.testengineering.html.pages.LotusEditPage;
import com.workday.testengineering.html.utilities.TestUtil;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class PositionCreatePage extends LotusEditPage {

	private static final By JOB_POSTING_TITLE = By.id("wd-TextInput-56$20194");
	private static final By AVAILABILITY_DATE = By.id("wd-Date-56$9735");
	private static final By EARLIEST_HIRE_DATE = By.id("wd-Date-56$9736");
	private static final String NO_JOB_RESTRICTIONS = "wd-CheckBox-56$17934";
	private static final By ENTER_COMMENT = By.cssSelector("div[id='wd-TextArea-56$11381'] textarea");
	private static final By NUMBER_OF_POSITIONS = By.xpath(div()+withId("wd-Numeric-56$20192")+input());
	private static final String JOB_FAMILY = "wd-MonikerInput-56$17935";
	private static final String JOB_PROFILE = "wd-MonikerInput-56$17937";
	private static final String LOCATION = "wd-MonikerInput-56$17938";
	private static final String TIME_TYPE = "wd-MonikerInput-56$17932";
	private static final String WORKER_TYPE = "wd-MonikerInput-56$17939";
	private static final String WORKER_SUBTYPE = "wd-MonikerInput-56$111049";
	private static final String POSITION_CREATE_PAGE_ID = "wd-EditPage-6$701";
	private static final By JOB_DESCRIPTION_FIELD = By.xpath(div() + withId("wd-TextArea-56$20853") + textInputArea());

	@Inject
	public PositionCreatePage(WebDriver driver, PollingAssistant assistant){
		super(driver, assistant, POSITION_CREATE_PAGE_ID);
	}

	private HtmlElement jobPostingTitleField() {
		return element(JOB_POSTING_TITLE);
	}
	
	public PositionCreatePage enterJobPostingTitle(String text){
		waitUntil(jobPostingTitleField(), is(displayed()));
		fillText(JOB_POSTING_TITLE, text);
		return this;
	}
			
	public PositionCreatePage enterAvailabilityDate(String date){
		dateControl(TYPE.DAY, AVAILABILITY_DATE).enterFirstInput(date);
		waitFor(shortTime);
		return this;
	}
	
	public PositionCreatePage enterEarliestHireDate(String date){		
		dateControl(TYPE.DAY, EARLIEST_HIRE_DATE).enterFirstInput(date);
		waitFor(shortTime);
		return this;
	}
	
	public PositionCreatePage enterJobDescription(String description) {
		when(element(JOB_DESCRIPTION_FIELD), is(displayed())).click();
		fillText(JOB_DESCRIPTION_FIELD, description);
		TestUtil.pause();
		return this;
	}
	
	public PositionCreatePage checkNoJobRestrictionsCheckBox(){
		when(element(By.id(NO_JOB_RESTRICTIONS)), is(displayed())).scrollIntoView();
		when(element(By.xpath(span() + withId(NO_JOB_RESTRICTIONS))), is(displayed())).click();
		return this;
	}
	
	public PositionCreatePage uncheckNoJobRestrictions(){
		checkBox(NO_JOB_RESTRICTIONS).uncheck();
		return this;
	}
	
	public PositionCreatePage enterComment(String comment){
		fillText(ENTER_COMMENT, "" + comment);
		return this;
	}

	private LotusRegularPromptWithMultiSelection jobFamilyPrompt() {
		return regularPromptWithMultiSelection(JOB_FAMILY);
	}
	
	public PositionCreatePage selectJobFamily(String jobGroup, String jobFamily) {
		LotusRegularPromptWithMultiSelection jobFamilyPrompt = jobFamilyPrompt();
		jobFamilyPrompt.open();
		jobFamilyPrompt.selectTopLevelOption(jobGroup);
		jobFamilyPrompt.selectOption(jobFamily);
		TestUtil.pauseFor(3000);
		return this;
	}
	
	public PositionCreatePage selectJobProfile(String value){
		LotusRegularPromptWithMultiSelection prompt = regularPromptWithMultiSelection(JOB_PROFILE);
		prompt.open();
		prompt.selectByInnerSearch(value);
		return this;
	}
	
	public PositionCreatePage selectLocation(String value){
		LotusRegularPromptWithMultiSelection prompt = regularPromptWithMultiSelection(LOCATION);
		prompt.open();
		prompt.selectByInnerSearch(value);
		return this;
	}
	
	public PositionCreatePage selectTimeType(String value){
		LotusRegularPromptWithSingleSelection prompt = regularPromptWithSingleSelection(TIME_TYPE);
		prompt.open();
		prompt.selectByInnerSearch(value);
		return this;
	}
	
	public PositionCreatePage selectWorkerType(String value){
		LotusRegularPromptWithSingleSelection prompt = regularPromptWithSingleSelection(WORKER_TYPE);
		prompt.open();
		prompt.selectByInnerSearch(value);
		return this;
	}
	
	public PositionCreatePage selectWorkerSubType(String value){
		LotusRegularPromptWithMultiSelection prompt = regularPromptWithMultiSelection(WORKER_SUBTYPE);
		prompt.open();
		prompt.selectByInnerSearch(value);
		return this;
	}
	
	public PositionCreatePage enterNumberOfPositions(String text){
		waitUntil(element(NUMBER_OF_POSITIONS), is(displayed()));
		fillNumericTextWithoutClearing(element(NUMBER_OF_POSITIONS), text);
		return this;
	}
}
