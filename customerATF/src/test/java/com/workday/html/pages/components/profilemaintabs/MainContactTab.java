package com.workday.html.pages.components.profilemaintabs;

import org.openqa.selenium.WebDriver;

import com.workday.html.pages.components.profilesubtabs.ContactTabDetails;
import com.workday.testengineering.html.component.ProfileTopLevelTab;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class MainContactTab extends ProfileTopLevelTab {
	
	private static final String CONTACT_TAB = "Contact";

	public MainContactTab(WebDriver driver, PollingAssistant assistant) {
		super(driver, assistant);
	}
	
	public MainContactTab open() {
		return open(CONTACT_TAB, this);
	}
	
	public ContactTabDetails contactTab() {
		return new ContactTabDetails(driver(), pollingAssistant());
	}
}
