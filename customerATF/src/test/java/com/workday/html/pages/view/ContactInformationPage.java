package com.workday.html.pages.view;

import javax.inject.Inject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.workday.testengineering.html.pages.ViewPage;
import com.workday.testengineering.shared.polling.PollingAssistant;

/**
 * This page may have different titles, depending on context in which it has been opened.
 */
public class ContactInformationPage extends ViewPage {

	@Inject
	public ContactInformationPage(WebDriver driver, PollingAssistant assistant) {
		super(driver, assistant, PAGE_ID);
	}
	
	public ContactInformationPage clickEditButton() {
		safelyClick(EDIT_BUTTON);
		return this;
	}

	private static final String PAGE_ID = "wd-ViewPage-56$18505";
	private static final By EDIT_BUTTON = By.linkText("Edit");
}