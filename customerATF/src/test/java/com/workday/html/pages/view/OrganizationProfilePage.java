package com.workday.html.pages.view;

import static com.workday.testengineering.html.core.HtmlElementExpressions.displayed;

import javax.inject.Inject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.workday.testengineering.html.component.LotusRelatedAction;
import com.workday.testengineering.html.pages.ViewPage;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class OrganizationProfilePage extends ViewPage {

	private static final String VIEW_ORGANIZATION_PAGE_ID = "wd-ViewPage-56$243553";
	
	private static final By PROFILE_RELATED_ACTION_BUTTON = by(xpath().withIdContains("wd-LotusProfile").with("span").withText("Actions"));

	@Inject
	public OrganizationProfilePage(WebDriver driver, PollingAssistant assistant) {
		super(driver, assistant, VIEW_ORGANIZATION_PAGE_ID);
	}
	
	public void openFromRelatedAction(String mainAction, String subAction) {
		LotusRelatedAction relatedAction = new LotusRelatedAction(driver(), pollingAssistant());
		when(element(PROFILE_RELATED_ACTION_BUTTON), is(displayed())).click();
		relatedAction.forMainAction(mainAction).selectSubAction(subAction);
	}

}