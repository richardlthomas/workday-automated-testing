package com.workday.html.pages.components.profilemaintabs;

import org.openqa.selenium.WebDriver;

import com.workday.html.pages.components.profilesubtabs.PersonalInformationTabDetails;
import com.workday.testengineering.html.component.ProfileTopLevelTab;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class MainPersonalTab extends ProfileTopLevelTab {

	private static final String PERSONAL_TAB = "Personal";

	public MainPersonalTab(WebDriver driver, PollingAssistant assistant) {
		super(driver, assistant);
	}

	public MainPersonalTab open() {
		return open(PERSONAL_TAB, this);
	}
	
	public PersonalInformationTabDetails personalInformationTab() {
		return new PersonalInformationTabDetails(driver(), pollingAssistant());
	}
}
