package com.workday.html.pages.edit;

import javax.inject.Inject;

import org.openqa.selenium.WebDriver;

import com.workday.testengineering.html.pages.LotusEditPage;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class ContactInformationStartPage extends LotusEditPage {

	@Inject
	public ContactInformationStartPage(WebDriver driver, PollingAssistant assistant) {
		super(driver, assistant, PAGE_ID);
	}

	public ContactInformationStartPage selectWorker(String worker) {
		regularPromptWithSingleSelection(WORKER_PROMPT)
			.open()
			.selectByInnerSearch(worker);
		return this;
	}
	
	private static final String PAGE_ID = "wd-EditPage-6$6344";
	private static final String WORKER_PROMPT = "wd-MonikerInput-56$22031";
}