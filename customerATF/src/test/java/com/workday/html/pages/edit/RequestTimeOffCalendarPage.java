package com.workday.html.pages.edit;

import static com.workday.testengineering.html.core.HtmlElementExpressions.displayed;
import static com.workday.testengineering.html.core.HtmlElementExpressions.enabled;

import javax.inject.Inject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.workday.testengineering.html.pages.LotusEditPage;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class RequestTimeOffCalendarPage extends LotusEditPage {

	@Inject
	public RequestTimeOffCalendarPage(WebDriver driver, PollingAssistant assistant) {
		super(driver, assistant, REQUEST_TIME_OFF_CALENDAR_PAGE_ID);
	}
	
	public Boolean isLoaded() {
		return (the(element(BALANCE_SIDE_PAGE), is(displayed())) && the(element(LOADING_SPINNER), not(displayed())) && the(element(By.id(REQUEST_TIME_OFF_CALENDAR_PAGE_ID)), is(displayed())));
	}
	
	public RequestTimeOffCalendarPage clickSelectDayOfMonth(int dayOfMonth){
	    requestTimeOffCalendar().selectDay(dayOfMonth);
	    return this;
	}
	
	public RequestTimeOffCalendarPage requestSelectedTimeOff(){
	    when(element(REQUEST_TIME_OFF_BUTTON), is(enabled())).click();
	    return this;
	}

	private static final String REQUEST_TIME_OFF_CALENDAR_PAGE_ID = "wd-ViewPage-6$7673";
	private static final By REQUEST_TIME_OFF_BUTTON = By.linkText("Request Time Off");
	private static final By LOADING_SPINNER = By.xpath("//img[@automationid='loadingSpinner']");
	private static final By BALANCE_SIDE_PAGE = By.id("wd-EditPage-6$57675");
}
