package com.workday.html.pages.edit;

import javax.inject.Inject;

import org.openqa.selenium.WebDriver;

import com.workday.testengineering.html.component.BPFToolbar;
import com.workday.testengineering.html.pages.LotusEditPage;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class ApproveCreatePositionEditPage extends LotusEditPage {

	private BPFToolbar bpfToolbar;

	@Inject
	public ApproveCreatePositionEditPage(WebDriver driver, PollingAssistant assistant, String pageId, BPFToolbar bpfToolbar) {
		super(driver, assistant, "wd-EditPage-6$703");
		this.bpfToolbar = bpfToolbar;
	}
	
	public ApproveCreatePositionEditPage approve(String comment) {
		bpfToolbar.approve(comment);
		return this;
	}

	public ApproveCreatePositionEditPage approve() {
		bpfToolbar.approve();
		return this;
	}
}
