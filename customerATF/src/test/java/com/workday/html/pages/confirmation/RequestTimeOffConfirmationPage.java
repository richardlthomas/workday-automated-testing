package com.workday.html.pages.confirmation;

import javax.inject.Inject;

import org.openqa.selenium.WebDriver;

import com.workday.testengineering.html.pages.LotusConfirmationPage;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class RequestTimeOffConfirmationPage extends LotusConfirmationPage {

	private static final String PAGE_ID = "wd-ConfirmPage-56$79586";
	
	@Inject
	public RequestTimeOffConfirmationPage(WebDriver driver, PollingAssistant assistant) {
		super(driver, assistant, PAGE_ID);
	}
}
