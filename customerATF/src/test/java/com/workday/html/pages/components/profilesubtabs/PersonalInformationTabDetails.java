package com.workday.html.pages.components.profilesubtabs;

import static com.workday.testengineering.html.core.HtmlElementExpressions.displayed;
import static com.workday.testengineering.shared.waiting.Within.within;
import static java.util.concurrent.TimeUnit.SECONDS;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.workday.testengineering.html.component.ProfileSubLevelTab;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class PersonalInformationTabDetails extends ProfileSubLevelTab {

	private static final String PERSONAL_INFORMATION_TAB = "Personal Information";
	private static final By EDIT_BUTTON = By.xpath(div() + withId("wd-DropDownCommandButton-56$83530") + link());
	private static final By EDIT_BUTTON_2 = By.xpath(div() + withId("wd-DropDownCommandButton-56$83528") + link());

	public PersonalInformationTabDetails(WebDriver driver, PollingAssistant assistant) {
		super(driver, assistant);
	}
	
	public PersonalInformationTabDetails open() {
		return open(PERSONAL_INFORMATION_TAB, this);
	}

	public PersonalInformationTabDetails edit() {
		if(the(element(EDIT_BUTTON), within(30, SECONDS), is(displayed()))) {
			when(element(EDIT_BUTTON), is(displayed())).click();
		} else {
			when(element(EDIT_BUTTON_2), is(displayed())).click();
		}
		return this;
	}
}
