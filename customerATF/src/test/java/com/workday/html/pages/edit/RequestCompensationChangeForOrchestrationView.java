package com.workday.html.pages.edit;

import static com.workday.testengineering.html.core.HtmlElementExpressions.displayed;

import javax.inject.Inject;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import com.workday.testengineering.html.core.HtmlElement;
import com.workday.testengineering.html.pages.LotusEditPage;
import com.workday.testengineering.html.utilities.TestUtil;
import com.workday.testengineering.shared.polling.PollingAssistant;

public class RequestCompensationChangeForOrchestrationView extends LotusEditPage {
	
	private static final String PAGE_ID = "wd-ViewPage-1$5574";
	private static final String SALARY = "wd-ActiveList-6$47729";
	private static final By AMOUNT = by(xpath().with("*").withId("wd-Currency-56$158112").with("input"));

	@Inject
	public RequestCompensationChangeForOrchestrationView(WebDriver driver,PollingAssistant assistant, String pageId) {
		super(driver, assistant, PAGE_ID);
	}
	
	public RequestCompensationChangeForOrchestrationView enterSalary(String amount) {
		orchestrationRow(SALARY).editNoChange();
		clearAmountField();
		amount().sendKeys(amount);
		TestUtil.pause();
		return this;
	}

	private void clearAmountField() {
		when(amount(), is(displayed())).click();
		TestUtil.pauseFor(250);
		when(amount(), is(displayed())).clear();
		TestUtil.pauseFor(250);
		String initialNumericValue = amount().getAttribute("value");
		
		for (int i = 0; i <= initialNumericValue.length(); i++) {
			amount().sendKeys(Keys.BACK_SPACE);
			TestUtil.pauseFor(250);
		}
		
		TestUtil.pause();
	}

	private HtmlElement amount() {
		return element(AMOUNT);
	}
}
