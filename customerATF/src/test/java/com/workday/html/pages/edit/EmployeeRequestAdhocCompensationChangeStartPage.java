package com.workday.html.pages.edit;

import static com.workday.testengineering.shared.waiting.Timer.midTime;
import static com.workday.testengineering.shared.waiting.Timer.waitFor;

import javax.inject.Inject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.workday.testengineering.html.component.DateControlFactory.TYPE;
import com.workday.testengineering.html.component.LotusRegularPromptWithSingleSelection;
import com.workday.testengineering.html.pages.LotusEditPage;
import com.workday.testengineering.shared.polling.PollingAssistant;
import com.workday.testengineering.shared.utilities.DateString;

public class EmployeeRequestAdhocCompensationChangeStartPage extends LotusEditPage {
	private static final String PAGE_ID = "wd-EditPage-6$2207";
	private static final By EFFECTIVE_DATE_DATE = By.id("wd-Date-56$86329");
	private static final String REASON_PROMPT = "wd-MonikerInput-56$168610";
	private static final String EMPLOYEE_PROMPT = "wd-MonikerInput-56$6747";
	private static final String POSITION_PROMPT = "wd-MonikerInput-56$97569";

	@Inject
	public EmployeeRequestAdhocCompensationChangeStartPage(WebDriver driver, PollingAssistant assistant) {
		super(driver, assistant, PAGE_ID);
	}

	public EmployeeRequestAdhocCompensationChangeStartPage enterEffectiveDate(String date) {
		String dateInput;
		if (date.equals("today")) {
			dateInput = DateString.today();
		} else {
			dateInput = date;
		}
		dateControl(TYPE.DAY, EFFECTIVE_DATE_DATE).enterFirstInput(dateInput);
		return this;
	}

	public EmployeeRequestAdhocCompensationChangeStartPage selectReason(String firstSearch, String secondSearch) {
		LotusRegularPromptWithSingleSelection reason = regularPromptWithSingleSelection(REASON_PROMPT);
		reason.open();
		waitFor(midTime);
		reason.selectSubLevelOption(firstSearch);
		reason.selectOptionWithoutWaitingForSelection(secondSearch);
		return this;
	}

	public EmployeeRequestAdhocCompensationChangeStartPage selectEmployee(String empName) {
		LotusRegularPromptWithSingleSelection employee = regularPromptWithSingleSelection(EMPLOYEE_PROMPT);
		employee.open();
		waitFor(midTime);
		employee.selectByInnerSearch(empName);
		return this;
	}

	public EmployeeRequestAdhocCompensationChangeStartPage selectPosition(String position) {
		LotusRegularPromptWithSingleSelection posPrompt = regularPromptWithSingleSelection(POSITION_PROMPT);
		posPrompt.open();
		waitFor(midTime);
		posPrompt.selectOption(position);
		return this;
	}
}