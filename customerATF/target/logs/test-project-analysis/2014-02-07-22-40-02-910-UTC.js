{
  "reportDate": "Feb 7, 2014 5:40:02 PM",
  "classes": [
    {
      "className": "com.workday.tests.DataDrivenTimeOffTests",
      "testAnnotation": null,
      "methods": [
        {
          "methodName": "requestTimeOff(java.lang.String,int,java.lang.String,java.lang.String)",
          "testAnnotation": {
            "dataProvider": "cellsTest",
            "dataProviderClass": "java.lang.Object",
            "dependsOnGroups": [],
            "enabled": true,
            "groups": [
              "exampleTest"
            ],
            "dependsOnMethods": [],
            "expectedExceptions": [],
            "expectedExceptionsMessageRegExp": ".*",
            "ignoreMissingDependencies": false,
            "invocationCount": 1,
            "invocationTimeOut": 0,
            "priority": 0,
            "retryAnalyzer": "java.lang.Class",
            "singleThreaded": false,
            "skipFailedInvocations": false,
            "successPercentage": 100,
            "suiteName": "",
            "testName": "",
            "threadPoolSize": 0,
            "timeOut": 0
          }
        }
      ]
    },
    {
      "className": "com.workday.tests.PositionTests",
      "testAnnotation": null,
      "methods": [
        {
          "methodName": "createPosition()",
          "testAnnotation": {
            "dataProvider": "",
            "dataProviderClass": "java.lang.Object",
            "dependsOnGroups": [],
            "enabled": true,
            "groups": [
              "exampleTest"
            ],
            "dependsOnMethods": [],
            "expectedExceptions": [],
            "expectedExceptionsMessageRegExp": ".*",
            "ignoreMissingDependencies": false,
            "invocationCount": 1,
            "invocationTimeOut": 0,
            "priority": 0,
            "retryAnalyzer": "java.lang.Class",
            "singleThreaded": false,
            "skipFailedInvocations": false,
            "successPercentage": 100,
            "suiteName": "",
            "testName": "",
            "threadPoolSize": 0,
            "timeOut": 0
          }
        },
        {
          "methodName": "approvePosition()",
          "testAnnotation": {
            "dataProvider": "",
            "dataProviderClass": "java.lang.Object",
            "dependsOnGroups": [],
            "enabled": true,
            "groups": [
              "exampleTest"
            ],
            "dependsOnMethods": [
              "createPosition"
            ],
            "expectedExceptions": [],
            "expectedExceptionsMessageRegExp": ".*",
            "ignoreMissingDependencies": false,
            "invocationCount": 1,
            "invocationTimeOut": 0,
            "priority": 0,
            "retryAnalyzer": "java.lang.Class",
            "singleThreaded": false,
            "skipFailedInvocations": false,
            "successPercentage": 100,
            "suiteName": "",
            "testName": "",
            "threadPoolSize": 0,
            "timeOut": 0
          }
        }
      ]
    },
    {
      "className": "com.workday.tests.ChangeCompensationTests",
      "testAnnotation": null,
      "methods": [
        {
          "methodName": "changeCompensation()",
          "testAnnotation": {
            "dataProvider": "",
            "dataProviderClass": "java.lang.Object",
            "dependsOnGroups": [],
            "enabled": true,
            "groups": [
              "exampleTest"
            ],
            "dependsOnMethods": [],
            "expectedExceptions": [],
            "expectedExceptionsMessageRegExp": ".*",
            "ignoreMissingDependencies": false,
            "invocationCount": 1,
            "invocationTimeOut": 0,
            "priority": 0,
            "retryAnalyzer": "java.lang.Class",
            "singleThreaded": false,
            "skipFailedInvocations": false,
            "successPercentage": 100,
            "suiteName": "",
            "testName": "",
            "threadPoolSize": 0,
            "timeOut": 0
          }
        }
      ]
    },
    {
      "className": "com.workday.tests.ChangeContactInformationTests",
      "testAnnotation": null,
      "methods": [
        {
          "methodName": "changeAddress()",
          "testAnnotation": {
            "dataProvider": "",
            "dataProviderClass": "java.lang.Object",
            "dependsOnGroups": [],
            "enabled": true,
            "groups": [
              "exampleTest"
            ],
            "dependsOnMethods": [],
            "expectedExceptions": [],
            "expectedExceptionsMessageRegExp": ".*",
            "ignoreMissingDependencies": false,
            "invocationCount": 1,
            "invocationTimeOut": 0,
            "priority": 0,
            "retryAnalyzer": "java.lang.Class",
            "singleThreaded": false,
            "skipFailedInvocations": false,
            "successPercentage": 100,
            "suiteName": "",
            "testName": "",
            "threadPoolSize": 0,
            "timeOut": 0
          }
        }
      ]
    }
  ]
}